package adder;

interface ADDER;
  method Action add(Bit#(1) a, Bit#(1) b, Bit#(1) cin);
  method ActionValue #(Bit#(1)) getSum;
  method ActionValue #(Bit#(1)) getCarry;
endinterface

module adder(ADDER);
  Reg#(Bool) busy_flag_s <- mkReg(False);
  Reg#(Bool) busy_flag_c <- mkReg(False);

  Reg#(Bit#(1)) s <- mkReg(0);
  Reg#(Bit#(1)) cout <- mkReg(0);

  method Action add(Bit#(1) a, Bit#(1) b, Bit#(1) cin) if(!busy_flag_s && !busy_flag_c);
     busy_flag_s <= True;
     busy_flag_c <= True;

     s <= a ^ b ^ cin;
     cout <= (a & b) | (a & cin) | (b & cin);
  endmethod

  method ActionValue #(Bit#(1)) getSum if (busy_flag_s);
     busy_flag_s <= False;
    return s; 
  endmethod

  method ActionValue #(Bit#(1)) getCarry if (busy_flag_c);
     busy_flag_c <= False;
    return cout; 
  endmethod

endmodule

endpackage
