package rippleTb;
import ripple :: *;

module rippleTb();
   Reg#(Bit#(8)) x1 <- mkReg(0);
    Reg#(Bit#(8)) y1 <- mkReg(0);
    Reg#(Bit#(8)) status <- mkReg(0);
    Reg#(Bit#(8)) count <- mkReg(0);
    FA g <- ripple();






   rule calculate if (count < 5);
      x1 <=y1 + 3;
      y1 <=x1 + 6;
      count <= count+1;

      g.add(x1, y1);
  $display("Add of %d   %d  =", x1, y1);

    endrule

    rule print if (count > 0 && status < 5);

      $display(" Add %d", g.getresult());
       status <= status + 1;

    endrule



    endmodule
endpackage: rippleTb

