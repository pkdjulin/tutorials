

//import "Libraries/BSV/Helpers.bsv";
package ripple;

interface FA;
  method Action add(Bit#(8) a, Bit#(8) b);
  method ActionValue #(Bit#(9)) getresult;
endinterface

module ripple(FA);

    Reg#(Bit#(8)) x <- mkReg(0);
    Reg#(Bit#(8)) y <- mkReg(0);
    Reg#(Bit#(9)) sum <- mkReg(0);
    Reg#(Bit#(9)) carry <- mkReg(0);
    Reg#(Bit#(4)) n     <- mkReg(0);
    Reg#(Bool) busy_flag <- mkReg(False);


  rule adder if (busy_flag && n < 9);
  if(n<8) begin
    sum[n]      <= (x[n] ^ y[n]) ^ carry[n] ;

    carry[n+1]  <= (x[n] & y[n])  | ( carry[n] & ( x[n] ^ y[n] ) ) ;
    n           <= n + 1;
  end
  else
    sum[n] <= carry[n];

  endrule

  method Action add(Bit#(8) a, Bit#(8) b )if (!busy_flag);
    x <= a;
    y <= b;
    busy_flag <= True;
    n <= 0;
    carry <= 0;
    sum     <= 0;
  endmethod

  method ActionValue #(Bit#(9)) getresult if (busy_flag && (n==8));

    busy_flag   <=  False;
    sum[n]      <=  carry[n] ;
    return          sum;
  endmethod :getresult


endmodule


endpackage : ripple


