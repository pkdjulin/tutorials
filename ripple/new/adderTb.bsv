package adderTb;
import adder :: *;

module adderTb();
    Reg#(Bit#(1)) x1 <- mkReg(0);
    Reg#(Bit#(1)) y1 <- mkReg(0);
    Reg#(Int#(8)) sel <- mkReg(0);
    Reg#(Bit#(1)) count <- mkReg(0);

    ADDER g <- adder();


   rule calculate;
     case (sel)
       0: g.add(0, 0, 0);
       1: g.add(0, 0, 1);
       2: g.add(0, 1, 0);
       7: g.add(1, 1, 1);
     endcase

      //g.add(0, 0, 0);
      //g.add(0, 0, 1);
      //g.add(0, 1, 0);
      //g.add(0, 1, 1);
      //g.add(1, 0, 0);
      //g.add(1, 0, 1);
      //g.add(1, 1, 0);
      //g.add(1, 1, 1);
    endrule

    rule print;
      $display(" Result: %d%d" , g.getCarry(), g.getSum());
      sel <= sel+1;
    endrule

    endmodule
endpackage: adderTb

